# Just a script to compile and run a single .java file that has no connections
# outside the file (like public classes or imports of other files / libraries)
# You must have JRE and JDK installed
# Usage: jav [path/to/file.java]
function jav() {
    if [ -z $1 ]; then
        echo "Usage: jav [dir/to/file.java] || jav [dir/to/main.java] [classes.txt]";
        return;
    fi;

    if [ -z $2 ]; then
        if [ -f $1 ]; then
            if [ ${1:$(expr ${#1} - 5):5} == ".java" ]; then
                javac $1;
                java ${1:0:$(expr ${#1} - 5)};
                rm "${1:0:$(expr ${#1} - 5)}.class";
                return;
            fi;
            echo "Must be a .java file";
            return;
        fi;
        echo "File doesn't exist";
        return;
    fi;

    if [ -z $3 ]; then
        if [ -f $2 ]; then
            if [ ${2:$(expr ${#2} - 4):4} == ".txt" ]; then
                classpath="";
                while read line; do
                    classpath+="\"${line}\"":;
                done < $2;
                javac "${1}" -cp "${classpath}.";
                java "${1}" "${classpath}";

                while read line; do
                    rm "${line:0:$(expr ${#line} - 5)}.class";
                done < $2;
                return;
            fi;
            echo "Second argument must be a .txt file"
            return;
        fi;
        echo "That file doesn't exist: ${2}";
        return;
    fi;
    echo "Takes a maximum of 2 arguments";
}